<?php

namespace Drupal\stripe_pay\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the 'stripe_payment' formatter.
 *
 * @FieldFormatter(
 *   id = "stripe_payment_default_formatter",
 *   module = "stripe_pay",
 *   label = @Translation("Default Formatter"),
 *   field_types = {
 *     "stripe_payment"
 *   }
 * )
 */
class StripePaymentDefaultFormatter extends FormatterBase {
  /**
   * The default currency unit.
   *
   * @var string
   */
  protected $defaultUnit = "USD";

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor for the custom field formatter plugin.
   *
   * @param string $plugin_id
   *   The plugin ID for this formatter.
   * @param array $plugin_definition
   *   The plugin definition array containing plugin-specific configuration.
   * @param \Drupal\field\FieldDefinitionInterface $field_definition
   *   The field definition object, which provides information about
   *   the field being formatted.
   * @param array $settings
   *   An array of settings that configure the formatter, typically
   *   coming from the plugin settings.
   * @param string $label
   *   The label for the formatter, used to identify it in the UI.
   * @param string $view_mode
   *   The view mode (e.g., "default", "teaser") the formatter is being
   *   applied to.
   * @param array $third_party_settings
   *   Any third-party settings provided by modules that extend the formatter.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service, used to retrieve configuration
   *   values (like stripe payment settings).
   * @param \Symfony\Component\HttpFoundation\RequestStack|null $request_stack
   *   (Optional) The request stack.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\FormatterPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $currency_code = 'USD';

    return [
      'currency_code' => $currency_code,
      'price_format' => 'format1',
      'show_quantity' => FALSE,
      'button_text' => 'Pay Now',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $stripe_pay_config = $this->configFactory->get('stripe_pay.settings');

    $currency_code = $stripe_pay_config->get('currency_code') ?? 'USD';

    $currency_sign = stripe_pay_currency_sign($currency_code);
    $element = parent::settingsForm($form, $form_state);
    $price_formats = stripe_pay_price_formats($currency_code, $currency_sign);

    $element['price_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Price Format'),
      '#description' => $this->t('Select the price format'),
      '#options' => $price_formats,
      '#default_value' => $this->getSetting('price_format') ?? '',
      '#required' => FALSE,
    ];

    $element['show_quantity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show quantity input field'),
      '#description' => $this->t('Display quantity field'),
      '#default_value' => $this->getSetting('show_quantity') ?? FALSE,
    ];

    $element['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#description' => $this->t('Enter the button text'),
      '#default_value' => $this->getSetting('button_text') ?? 'Pay Now',
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $stripe_pay_config = $this->configFactory->get('stripe_pay.settings');

    $currency_code = $stripe_pay_config->get('currency_code');

    $price_format = $this->getSetting('price_format');
    $show_quantity = $this->getSetting('show_quantity');
    $button_text = $this->getSetting('button_text');

    $show_quantity = $show_quantity ? 'True' : 'False';

    $currency_sign = stripe_pay_currency_sign($currency_code);
    $price_formats = stripe_pay_price_formats($currency_code, $currency_sign);
    $price_format = $price_formats[$price_format];

    $summary_content = $this->t('Currency code: @currency_code<br>Price format: @price_format<br>Show quantity input field: @show_quantity<br>Button Text: @button_text', [
      '@currency_code' => $currency_code,
      '@price_format' => $price_format,
      '@show_quantity' => $show_quantity,
      '@button_text' => $button_text,
    ]);

    // Return the summary content wrapped in an array.
    return [$summary_content];
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $stripe_pay_config = $this->configFactory->get('stripe_pay.settings');

    $currency_code = $stripe_pay_config->get('currency_code');

    $price_format = $this->getSetting('price_format');
    $show_quantity = $this->getSetting('show_quantity');
    $button_text = $this->getSetting('button_text');

    $currency_sign = stripe_pay_currency_sign($currency_code);

    $current_url = $this->requestStack->getCurrentRequest()->getUri();

    foreach ($items as $delta => $item) {
      $node = $item->getEntity();
      $price = $item->stripe_payment;
      // $price = stripe_pay_convert_number_to_price(1234.56, 'USD');
      // Get the node title.
      $title = $node->label();
      $nid = $node->id();

      $stripe_payment_data = [
        'nid' => $nid,
        'title' => $title,
        'price' => $price,
        'current_url' => $current_url,
        'currency_code' => $currency_code,
        'show_quantity' => $show_quantity,
        'price_format' => $price_format,
        'button_text' => $button_text,
        'currency_sign' => $currency_sign,
      ];

      $other_data['suffix'] = [];
      $elements[$delta] = [
        '#theme' => 'stripe_payment',
        '#stripe_payment_data' => $stripe_payment_data,
        '#other_data' => $other_data,
      ];
    }

    $payment_init_url = Url::fromRoute('stripe_pay.payment_init', [], ['absolute' => TRUE])->toString();

    $stripe_pay_config = $this->configFactory->get('stripe_pay.settings');

    $currency_code = $stripe_pay_config->get('currency_code');
    $test_mode = $stripe_pay_config->get('test_mode');
    $publishable_key = '';
    if ($test_mode) {
      $publishable_key = $stripe_pay_config->get('publishable_key_test');
    }
    else {
      $publishable_key = $stripe_pay_config->get('publishable_key_live');
    }

    $elements['#attached']['library'][] = 'stripe_pay/stripe_pay';
    $elements['#attached']['drupalSettings']['payment_init_url'] = $payment_init_url;
    $elements['#attached']['drupalSettings']['stripe_pk'] = $publishable_key;

    return $elements;
  }

}
