<?php

namespace Drupal\stripe_pay\Controller;

use Stripe\StripeClient;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Defines StripeController class.
 */
class StripeController extends ControllerBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a StripeController object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, RequestStack $request_stack, MessengerInterface $messenger) {
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
  }

  /**
   * Callback URL handling for Sendinblue API API.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   Return markup for the page.
   */
  public function paymentInit(Request $request) {
    $payload = json_decode($request->getContent(), TRUE);
    $productPrice = $payload['amount'] ?? '1';
    $productName = $payload['title'] ?? 'Product without name';
    $url = $payload['url'] ?? '';
    $productID = $payload['id'] ?? '0';
    $quantity = $payload['quantity'] ?? '1';

    if ($productPrice < 1) {
      $productPrice = 1;
    }

    // Invoke the custom hook for success redirect.
    $this->moduleHandler->invokeAll('stripe_pay_success_redirect', [&$success_redirect_url]);

    if ($url == '') {
      $url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    }

    $success_redirect_url = Url::fromRoute('stripe_pay.payment_success', [], ['absolute' => TRUE])->toString();
    $cancel_redirect_url = Url::fromRoute('stripe_pay.payment_cancel', [], ['absolute' => TRUE])->toString();

    $stripe_pay_config = $this->configFactory->get('stripe_pay.settings');

    $test_mode = $stripe_pay_config->get('test_mode');
    $secret_key = '';
    if ($test_mode) {
      $secret_key = $stripe_pay_config->get('secret_key_test');
    }
    else {
      $secret_key = $stripe_pay_config->get('secret_key_live');
    }

    // Set API key.
    $stripe = new StripeClient($secret_key);

    $response_data = [
      'status' => 0,
      'error' => [
        'message' => 'Invalid Request!',
      ],
    ];

    // Retrieve the currency code from the configuration settings.
    // If it's not set or is null, default to 'USD' (U.S. Dollar).
    $currency = $stripe_pay_config->get('currency_code') ?? 'USD';

    // Here we assume $productPrice contains the price of the product in the
    // base currency (e.g., USD, EUR, etc.)
    // We multiply by 100 to convert the price into the smallest unit of the
    // currency (like cents).
    // This step is necessary because Stripe expects the amount in the
    // smallest currency unit.
    // For example: 10.50 USD (dollars) becomes 1050 cents for Stripe to process
    // the transaction correctly.
    $productPrice = $productPrice * 100;

    // Now, we round the product price to two decimal places to ensure
    // proper formatting, especially for currencies that work with
    // decimal values (e.g., USD, EUR, GBP).
    // This ensures no issues with floating point precision when
    // interacting with Stripe's API.
    $stripeAmount = round($productPrice, 2);

    // Create new Checkout Session for the order.
    try {
      $checkout_session = $stripe->checkout->sessions->create([
        'line_items' => [[
          'price_data' => [
            'product_data' => [
              'name' => $productName,
              'metadata' => [
                'pro_id' => $productID,
              ],
            ],
            'unit_amount' => $stripeAmount,
            'currency' => $currency,
          ],
          'quantity' => $quantity,
        ],
        ],
        'mode' => 'payment',
        'success_url' => $success_redirect_url . '?redirect=' . $url . '&session_id={CHECKOUT_SESSION_ID}',
        'cancel_url' => $cancel_redirect_url . '?redirect=' . $url,
      ]);
    }
    catch (\Exception $e) {
      $api_error = $e->getMessage();
      stripe_pay_log_messages('warning', $api_error);
    }

    if (empty($api_error) && $checkout_session) {
      $response_data = [
        'status' => 1,
        'message' => 'Checkout Session created successfully!',
        'sessionId' => $checkout_session->id,
      ];
    }
    else {
      $response_data = [
        'status' => 0,
        'error' => [
          'message' => 'Checkout Session creation failed! ' . $api_error,
        ],
      ];
    }

    // Create a JSON response.
    $response = new JsonResponse($response_data);

    return $response;

  }

  /**
   * Handles the payment success logic.
   *
   * This function is invoked when a payment is successfully processed.
   * It retrieves the session ID from the query parameters, executes
   * custom hooks for success redirection and success messages, and
   * sends a response message to the user.
   * If a custom redirect URL is not provided, it defaults to the front page.
   * Finally, the user is redirected to the appropriate URL after the
   * success message is displayed.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the appropriate URL.
   */
  public function paymentSuccess() {
    $session_id = $this->requestStack->getCurrentRequest()->query->get('session_id');

    $redirect_url = '';

    // Invoke the custom hook for success redirect.
    $this->moduleHandler->invokeAll('stripe_pay_success_redirect', [&$redirect_url]);

    $message = '';

    // Invoke the custom hook for cancel message.
    $this->moduleHandler->invokeAll('stripe_pay_success_message', [&$message]);

    if ($message == '') {
      $message = $this->t('<strong>Payment success!</strong> Your transaction has been successed with session ID: @session_id.', ['@session_id' => $session_id]);
    }

    // Add the success message to Drupal's messenger service.
    $this->messenger->addMessage($message);

    if ($redirect_url == '') {
      $redirect_url = $this->requestStack->getCurrentRequest()->query->get('redirect');
      if ($redirect_url == '') {
        $redirect_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      }
    }

    if ($session_id != '') {
      $response = new TrustedRedirectResponse($redirect_url);
      // Redirect to the external URL.
      $response->send();
    }
    else {
      $response = new TrustedRedirectResponse($redirect_url);
      $response->send();
    }
    return $response;
  }

  /**
   * Handles the payment cancellation logic.
   *
   * This function is invoked when a payment is canceled. It processes
   * the cancellation by checking if any custom redirect URL or message
   * is provided through hooks.
   * If not, it defaults to the front page for the redirect and a default
   * cancellation message. It also triggers the appropriate hooks for
   * custom redirect URLs and cancellation messages.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the appropriate URL after payment cancellation.
   */
  public function paymentCancel() {
    $cancel_redirect_url = '';

    // Invoke the custom hook for cancel redirect.
    $this->moduleHandler->invokeAll('stripe_pay_cancel_redirect', [&$cancel_redirect_url]);

    if ($cancel_redirect_url == '') {
      $cancel_redirect_url = $this->requestStack->getCurrentRequest()->query->get('redirect');
      if ($cancel_redirect_url == '') {
        $cancel_redirect_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      }
    }

    $message = '';

    // Invoke the custom hook for cancel message.
    $this->moduleHandler->invokeAll('stripe_pay_cancel_message', [&$message]);

    if ($message == '') {
      $message = $this->t('<strong>Payment Canceled!</strong> Your transaction has been canceled.');
    }

    $this->messenger->addMessage($message);

    $response = new TrustedRedirectResponse($cancel_redirect_url);
    $response->send();
    return $response;
  }

}
